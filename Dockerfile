FROM registry.access.redhat.com/rhel7/rhel:latest

RUN yum -y install httpd perl



# Simple startup script to avoid some issues observed with container restart 
ADD run-httpd.sh /run-httpd.sh
RUN chmod 777 /run-httpd.sh


ADD prova.html /var/www/html/


EXPOSE 80

ENTRYPOINT ["/run-httpd.sh"]

